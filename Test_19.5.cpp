﻿
#include <iostream>
using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "" << endl;
	}

};

class Cat : public Animal
{
	public:
		void Voice() override
		{
			cout <<"Cat voice:"<< "Meeeew!" << endl;
		}
};
class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Dog voice:"<<"Woof!" << endl;
	}
};
class People : public Animal
{
public:
	void Voice() override
	{
		cout << "People voice:"<<"Help!" << endl;
	}
};
	
int main()
{
	const int size = 3;
	Animal* animals[size] = {new Cat, new Dog, new People};
	for (Animal* a : animals)
		a->Voice();
}
